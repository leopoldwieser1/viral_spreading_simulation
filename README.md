# viral_spreading_simulation



## Overview

This is a tool, to simulate how people infect eachother over time. 

![](./plots/gifs/simulation.gif)
![](./plots/t_vs_infections_300_1000_100.png)
