# here I define rooms
# rooms are functions that return a boolean for a input position

def no_room(pos):
    return True


def circle_r20(pos):
    mp = [50, 50]
    r = 20
    return (pos[0] - mp[0]) ** 2 + (pos[1] - mp[1]) ** 2 <= r ** 2


def two_rooms(pos):
    a = (30 <= pos[0] < 70) and (0 <= pos[1] < 40)
    b = (30 <= pos[0] < 70) and (60 <= pos[1] < 100)
    c = (48 <= pos[0] < 52)
    return a or b or c
