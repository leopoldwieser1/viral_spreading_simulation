import os
import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import imageio

workdir = os.path.dirname(__file__)
sys.path.append(workdir)
from scripts.ClassPerson import Person
from scripts.ClassWorld import World
from scripts.rooms import no_room

world = World(100, no_room)


# main simulation methods

def initiate_simulation(n_people: int, world_size: int, room):
    global world
    world = World(world_size, room)
    people = [Person(idn=x, world=world) for x in range(n_people)]
    people[0].is_infected = True
    people[0].position = [world_size/2, world_size/2]
    time_line_df = pd.DataFrame({'time': [], 'infected': []})
    return people, time_line_df


def run_simulation(max_time, people, time_line_df):
    t = 0
    for i in range(max_time):
        t, people = time_step(t, people)
        time_line_df = time_line_df.append({'time': t, 'infected': count_infected(people)}, ignore_index=True)
    return people, time_line_df


def time_step(time, people, infection_distance=1.5, n=1):
    global world
    time += 1

    if time % n == 0:
        display_simulation(people, time, n)

    people_old = people[:]
    for person in people:
        if person.infection_time_counter == person.max_infection_time:
            person.is_infected = False
            person.is_cured = True
        if person.is_infected and not person.is_cured:
            person.infection_time_counter += 1
        person = infection(person, people_old, infection_distance)
        person.random_walk()


    return time, people


def infection(person, people, infection_distance):
    for p in people:
        if person.id != p.id and not person.is_infected and p.is_infected and not p.is_cured:
            if euclidean_distance(person.position, p.position) < infection_distance:
                person.is_infected = True
    return person


# auxiliary methods

def euclidean_distance(pos1, pos2):
    return np.linalg.norm(np.array(pos1) - np.array(pos2))


def count_infected(people):
    return sum([1 if p.is_infected else 0 for p in people])


# plotting methods

def display_simulation(people, t, n):
    global world
    x = [p.position[0] for p in people]
    y = [p.position[1] for p in people]
    color = ['r' if p.is_infected else 'b' if p.is_cured else 'g' for p in people]
    fig, ax = plt.subplots(figsize=(8, 8))
    scatter = plt.scatter(x, y, c=color)
    # plt.legend()
    plt.xlim(world.boundaries['x'])
    plt.ylim(world.boundaries['y'])
    plt.title(f't:{t}')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.savefig(os.path.join(os.path.dirname(workdir), 'plots', f'time_step_{int(t / n)}'))
    plt.close()
    # plt.show()


def display_infections(df, n_people, size):
    plt.plot(df['time'], df['infected'])
    plt.xlabel('time steps')
    plt.ylabel('infections')
    plt.savefig(os.path.join(os.path.dirname(workdir), 'plots', f't_vs_infections_{len(df)}_{n_people}_{size}'))
    plt.show()


def build_gif(t_max, plot_dir=os.path.join(os.path.dirname(workdir), 'plots')):
    with imageio.get_writer(os.path.join(plot_dir, 'gifs/simulation.gif'), mode='I') as writer:
        for filename in [os.path.join(plot_dir, f'time_step_{x + 1}.png') for x in range(t_max)]:
            image = imageio.imread(filename)
            writer.append_data(image)
            os.remove(filename)
