import os
import sys

workdir = os.path.dirname(__file__)
sys.path.append(workdir)
from scripts.methods import *
from scripts.rooms import *

epochs = 300
n_people = 1000
world_size = 100

people, time_line_df = initiate_simulation(n_people=n_people, world_size=world_size, room=no_room)

people, time_line_df = run_simulation(max_time=epochs, people=people, time_line_df=time_line_df)

display_infections(time_line_df, n_people, world_size)

build_gif(epochs)
