import numpy as np


class Person:
    def __init__(self, idn, world, walk_range=1, max_infection_time=30):
        self.id = idn
        self.world = world
        self.walk_range = walk_range
        pos = [world.size * np.random.random(), world.size * np.random.random()]
        while not world.room(pos):
            pos = [world.size * np.random.random(), world.size * np.random.random()]
        self.position = pos
        self.is_infected = False
        self.infection_time_counter = 0
        self.is_cured = False
        self.max_infection_time = max_infection_time

    def random_walk(self):
        """
        Lets Person walk into random direction
        """
        phi = 2.0 * np.pi * np.random.random()  # get random direction
        pos = self.walk_in_direction(phi, self.walk_range, self.position)
        while not Person.check_if_within_boundaries(pos, self.world):
            phi = np.pi / 2 + phi
            pos = self.walk_in_direction(phi, self.walk_range, self.position)

        self.position = pos

    @staticmethod
    def walk_in_direction(phi, walk_range, pos):
        x = walk_range * np.cos(phi) + pos[0]
        y = walk_range * np.sin(phi) + pos[1]
        return [x, y]

    @staticmethod
    def check_if_within_boundaries(pos, world):
        x = pos[0]
        y = pos[1]
        x_min, x_max = world.boundaries['x']
        y_min, y_max = world.boundaries['y']
        within_boundaries = x_min < x < x_max and y_min < y < y_max
        within_room = world.room(pos)
        if within_room and within_boundaries:
            return True
        else:
            return False
