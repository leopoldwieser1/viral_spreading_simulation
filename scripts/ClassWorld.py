class World:
    def __init__(self, size, room):
        self.room = room
        self.size = size
        self.boundaries = {'x': [0, size], 'y': [0, size]}
